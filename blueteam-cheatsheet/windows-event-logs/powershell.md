---
description: Microsoft-Windows-PowerShell/Operational
---

# PowerShell

<table data-full-width="true"><thead><tr><th>Event ID</th><th>Short Description</th><th>Type of Attack</th><th>Enabled by Default</th><th>Severity</th><th>MITRE Tactics</th><th>MITRE Techniques</th></tr></thead><tbody><tr><td>800</td><td>PowerShell pipeline execution start</td><td>Command Execution</td><td>No</td><td>High</td><td>Execution</td><td>Command-Line Interface (T1059)</td></tr><tr><td>4100</td><td>PowerShell engine state changed</td><td>Defense Evasion</td><td>No</td><td>High</td><td>Defense Evasion</td><td>Obfuscated Files or Information (T1027)</td></tr><tr><td>4103</td><td>PowerShell script block logging enabled</td><td>Logging</td><td>No</td><td>High</td><td>Collection</td><td>PowerShell (T1086)</td></tr><tr><td>4104</td><td>PowerShell script block logging disabled</td><td>Defense Evasion</td><td>No</td><td>High</td><td>Defense Evasion</td><td>Obfuscated Files or Information (T1027)</td></tr><tr><td>4105</td><td>PowerShell module logging enabled</td><td>Logging</td><td>No</td><td>High</td><td>Collection</td><td>PowerShell (T1086)</td></tr><tr><td>4106</td><td>PowerShell module logging disabled</td><td>Defense Evasion</td><td>No</td><td>High</td><td>Defense Evasion</td><td>Obfuscated Files or Information (T1027)</td></tr></tbody></table>
