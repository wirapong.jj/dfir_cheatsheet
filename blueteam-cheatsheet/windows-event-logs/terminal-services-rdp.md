---
description: Microsoft-Windows-TerminalServices-LocalSessionManager/Operational
---

# Terminal Services (RDP)

<table data-full-width="true"><thead><tr><th>Event ID</th><th>Short Description</th><th>Type of Attack</th><th>Enabled by Default</th><th>Severity</th><th>MITRE Tactics</th><th>MITRE Techniques</th></tr></thead><tbody><tr><td>21</td><td>A remote session was disconnected from the local session</td><td>Defense Evasion</td><td>Yes</td><td>Medium</td><td>Defense Evasion (TA0005)</td><td>Valid Accounts (T1078)</td></tr><tr><td>22</td><td>A remote session was logged off from the local session</td><td>Defense Evasion</td><td>Yes</td><td>Medium</td><td>Defense Evasion (TA0005)</td><td>Valid Accounts (T1078)</td></tr><tr><td>25</td><td>A remote session was reconnected to the local session</td><td>Defense Evasion</td><td>Yes</td><td>Medium</td><td>Defense Evasion (TA0005)</td><td>Valid Accounts (T1078)</td></tr><tr><td>41</td><td>A remote session was ended</td><td>Defense Evasion</td><td>Yes</td><td>Medium</td><td>Defense Evasion (TA0005)</td><td>Valid Accounts (T1078)</td></tr></tbody></table>
