---
description: SYSTEM
---

# System



<table data-full-width="true"><thead><tr><th>Event ID</th><th>Short Description</th><th>Type of Attack</th><th>Enabled by Default</th><th>Severity</th><th>MITRE Tactics</th><th>MITRE Techniques</th></tr></thead><tbody><tr><td>7031</td><td>A service was terminated unexpectedly</td><td>Malware</td><td>Yes</td><td>High</td><td>Defense Evasion (TA0005)</td><td>Service Stop (T1489)</td></tr><tr><td>7035</td><td>The Windows Modules Installer service entered the stopped state</td><td>Malware</td><td>Yes</td><td>High</td><td>Defense Evasion (TA0005)</td><td>Service Stop (T1489)</td></tr><tr><td>7036</td><td>The Volume Shadow Copy service entered the running state</td><td>Malware</td><td>Yes</td><td>High</td><td>Defense Evasion (TA0005)</td><td>Service Start (T1488)</td></tr><tr><td>7040</td><td>The start type of a service was changed</td><td>Malware</td><td>Yes</td><td>High</td><td>Defense Evasion (TA0005)</td><td>Modify Existing Service (T1543.003)</td></tr><tr><td>7045</td><td>A service was installed in the system</td><td>Malware</td><td>Yes</td><td>High</td><td>Defense Evasion (TA0005)</td><td>Service Execution (T1569.002)</td></tr><tr><td>7045</td><td>A service was installed in the system</td><td>Malware</td><td>Yes</td><td>High</td><td>Execution (TA0002)</td><td>Service Execution (T1569)</td></tr></tbody></table>
