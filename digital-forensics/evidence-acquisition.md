# Evidence Acquisition

สำหรับบทความนี้จะเน้นเกี่ยวกับพื้นฐานเกี่ยวกับการเก็บหลักฐานที่ใช้ในการทำ Digital Forensics and Incident Response ด้านเคสการโจมตีทาง Cyber Security เป็นหลัก\
\
\*\*ซึ่งหากเรื่องเคสที่จะต้องใช้หลักฐานในศาลจะมีขั้นตอนและรายละเอียดของขั้นตอนการเก็บหลักฐานที่แตกต่างจากนี้โดยสมควร

## Overview

### ประเภทของหลักฐาน&#x20;

สำหรับประเภทของหลักฐานที่ใช้หลักๆ จะมี 2 ประเภท

* **Volatile (หน่วยความจำชั่วคราว) :** หน่วยความจำประเภทที่จะต้องมีไฟฟ้าเลี้ยงตลอดเวลา หากไม่มีไฟฟ้าเลี้ยงข้อมูลที่ถูกเก็บไว้จะหายไป ยกตัวอย่างเช่น RAM\

* **Non-volatile (หน่วยความจำถาวร)** : เป็นหน่วยความจำที่สามารถเก็บข้อมูลไว้ได้ แม้จะไม่มีไฟเลี้ยง ยกตัวอย่างเช่น Harddisk (HDD) , Solid State drive (SSD), Flash, USB Thumb drive

### ข้อควรปฎิบัติทั่วไปในการเก็บหลักฐาน และวิเคราะห์หลักฐาน

* **ไม่ควรทำให้เกิดการเปลี่ยนแปลงของหลักฐาน**ในขั้นตอนการเก็บ เนื่องจากอาจทำให้กระทบต่อขั้นตอนการวิเคราะห์หลักฐานได้ หากมีก็ควรทำให้น้อยที่สุด และในขั้นตอนการวิเคราะห์หลักฐานจะต้องมีการป้องกันไม่ให้มีการเขียนข้อมูลอะไรก็ตามลงบนหลักฐาน
* **การเก็บและควบคุมการเข้าถึงหลักฐาน** จะต้องมีขั้นตอนการจัดการหลักฐานอย่างชัดเจน ตั้งแต่การเก็บหลักฐาน จนไปถึงการร้องขอหลักฐาน โดยการเก็บบันทึกข้อมูลผู้ที่ได้หลักฐาน (COC) เพื่อป้องกันไม่ให้เกิดการรั่วไหลของข้อมูล
* **ควรระวังการใช้และการวิเคราะห์ข้อมูลหลักฐานที่ Sensitive ไม่ให้เกิดการรั่วไหลของข้อมูล**

### อุปกรณ์ในการเก็บหลักฐาน

อุปกรณ์ในการเก็บหลักฐาน หลักๆมี 2 อย่าง

![Write Blocker](../.gitbook/assets/assets\_-LyIgjUG-vj8zcrmKlEV\_-Lzh5MKnWiDrnkgD7-3q\_-LzhAH04S2Aid8OkXTug\_image.webp)

* Write Blocker : ใช้สำหรับป้องกันการเขียน โดยจะใช้เพื่อป้องกันไม่ให้เกิดการเปลี่ยนแปลงของหลักฐาน (โดยส่วนใหญ่จะใช้ในกรณีที่ต้องการนำหลักฐานที่เก็บในรูปแบบ Disk มาวิเคราะห์)

![](../.gitbook/assets/assets\_-LyIgjUG-vj8zcrmKlEV\_-Lzh5MKnWiDrnkgD7-3q\_-LzhATSSG-mfFDY3Xsyb\_image.webp)

* Disk Duplicator : ใช้สำหรับการสำเนาหลักฐาน (โดยส่วนมากใช้ในการสำเนาหลักฐานในรูปแบบของ Disk to disk)

### วิธีการเก็บหลักฐานประเภท **Non-volatile (Disk)**

การเก็บหลักฐานเพื่อนำไปวิเคราะห์นั้น จะต้องเก็บโดยการสำเนาแบบ Bit by bit คือจะต้องเก็บทุก Bit ไม่ว่าพื้นที่นั้นจะถูกใช้หรือไม่ก็ตาม​&#x20;

{% hint style="info" %}
จะต้องสำเนาแบบ Bit by bit เพื่อให้มีความสามารถในการวิเคราะห์หลักฐานได้อย่างเต็มประสิทธิภาพ สามารถวิเคราะห์หลักฐานได้ทั้ง Allocated space, Unallocated space, Slack space (ในส่วนของประเภทของ Space จะมีอธิบายในบทความหน้า)
{% endhint %}

โดยมีวิธีการเก็บหลักฐานอยู่สองแบบคือ

* **Disk to disk :** เป็นการเก็บหลักฐานโดยทำการสำเนาจาก Disk ต้นทาง ไปยัง Disk ปลายทาง \
  (หากเก็บจาก Physical Disk เป็นลูก จะต้องใช้ Disk Duplicator เพื่อไม่ทำให้เกิดการเปลี่ยนแปลงของหลักฐาน)
* **Disk to image :** เป็นการเก็บหลักฐานโดยทำการสำเนาจาก Disk ต้นทาง ไปเป็นไฟล์ Disk Image (Raw, E01,..) ลงบนปลายทาง\
  (โดยทั่วไปการเก็บโดยวิธีนี้จะใช้ Software ในการเก็บหลักฐานเป็นไฟล์ Disk Image)

## การเก็บหลักฐานบนระบบปฎิบัติการ Windows&#x20;

โดยทั่วไปหากเกิด Cyber Incident แล้วจะต้องรีบเข้าไปเก็บหลักฐานทันที เพื่อที่จะเก็บหลักฐานในขณะที่ยังเกิดพฤติกรรมผิดปกติซึ่งเกิดจากการโดนโจมตีอยู่ ซึ่งจะทำให้มีประโยชน์ต่อการวิเคราะห์&#x20;

จะยกตัวอย่างการเก็บหลักฐานบน Windows โดยการใช้ FTK Imager Lite ซึ่งเป็นโปรแกรมเก็บหลักฐาน โดยสามารถเก็บได้ทั้ง Harddisk,Memory โดยทำงานในรูปแบบของ Portable สามารถ Run ได้โดยที่ไม่ต้อง Install ลงบนเครื่อง สามารถ Download ได้ที่ [FTK Imager Lite](https://accessdata.com/product-download/ftk-imager-lite-version-3-1-1)

โดยเราจะมาเตรียมตัวก่อน&#x20;

1. เตรียมตัว โดยก่อนอื่นเราจะนำโปรแกรม FTK Imager Lite ไปไว้บน External Storage เช่นพวก Frash Drive, External SSD ที่จะใช้ในการเก็บหลักฐาน เพื่อป้องกันการเขียนลงบนหลักฐานต้นฉบับ\
   **\*\*External Storage จะต้องมีพื้นที่มากพอที่จะสามารถเก็บหลักฐานได้**
2. เสียบ Portable Device ที่เราเตรียมมา ลงบนเครื่องที่ต้องการเก็บหลักฐาน
3. จะเริ่มจากการเก็บ Volatile (Memory) ก่อนเพื่อเก็บ State ต่างๆที่ Run อยู่ให้เร็วที่สุด จากนั้นค่อยทำการเก็บ Non-Volatile (Disk)

### **เก็บหลักฐานประเภท Volatile (Memory)**&#x20;

มีขั้นตอนการเก็บหลักฐานดังนี้

1. Run โปรแกรม FTK Imager Lite ด้วยสิทธิ์ Administrator โดยการคลิกขวา > Run as Administrator
2. เลือกที่ File > Capture memory (ตามภาพด้านล่าง)

![Capture memory](<../.gitbook/assets/image (6).png>)

&#x20; 2\. เลือก Destination path ว่าจะทำการเก็บ Memory ไปที่ไหน โดยเลือกบน External Storage เพื่อป้องกันการเขียนลงบนหลักฐานต้นฉบับ แล้วคลิกที่ Capture Memory

![Select destination path](<../.gitbook/assets/image (7) (1).png>)

&#x20; 3\. รอจนเสร็จ จากนั้นคลิกที่ Close ก็จะเสร็จสิ้นขั้นตอนการเก็บหลักฐานประเภท Volatile

![Memory capture finished](<../.gitbook/assets/image (8) (1).png>)

![Memdump file](<../.gitbook/assets/image (9) (1).png>)

### เก็บหลักฐานประเภท **Non-volatile (**Harddisk,SSD,..) แบบ Disk to image&#x20;

มีขั้นตอนการเก็บหลักฐานดังนี้

1. Run โปรแกรม FTK Imager Lite ด้วยสิทธิ์ Administrator โดยการคลิกขวา > Run as Administrator
2. ทำการเริ่มเก็บ Disk โดยเลือกที่ File > Create Disk Image... (ตามภาพด้านล่าง)

![Create Disk Image...](<../.gitbook/assets/image (10).png>)

&#x20; 3\. เลือกประเภทของ Source Evidence ว่าเราจะเก็บแบบไหน ซึ่งหลักๆแล้วจะใช้อยู่สองแบบคือ Physical Drive กับ Logical Drive (Drive C, Drive D,..) จากนั้น Next

![Source Evidence Type](<../.gitbook/assets/image (11).png>)

&#x20; 4\. เลือก Drive ที่ต้องการเก็บ จากนั้น Finish

![Source Drive Selection](<../.gitbook/assets/image (12).png>)

&#x20; 5\. ในขั้นตอนนี้จะต้องทำการเลือกว่าจะเก็บไฟล์ Image ไว้ที่ไหน คลิกที่ Add...

![Add Destination](<../.gitbook/assets/image (13) (1).png>)

&#x20; 6\. ให้เลือกประเภทของ Image โดยส่วนมากจะนิยมใช้ Raw, E01, AFF กัน ซึ่งข้อแตกต่างหลักๆก็จะเป็นในส่วนของเรื่องความสามารถในการบีบอัด (Compression) และ Metadata ต่างๆ โดยสามารถอ่านเพิ่มเติมได้ที่ [Category:Forensics File Formats](https://forensicswiki.xyz/wiki/index.php?title=Category:Forensics\_File\_Formats) หลังจากที่เลือกแล้วคลิกที่ Next >

![Select Image Type](<../.gitbook/assets/image (14) (1).png>)

&#x20; 7\. ใส่รายละเอียดข้อมูลเกี่ยวกับ Evidence นี้ จากนั้น Next >

![Evidence Information](<../.gitbook/assets/image (15).png>)

&#x20; 8\. ใส่ข้อมูลเกี่ยวกับ Image ที่เราจะทำการเก็บหลักฐานดังนี้\
&#x20;      8.1 **Image Destination Folder** **:** เลือกปลายทางที่จะใช้ในการเก็บ Evidence \
&#x20;               \*\*โดยจะเลือกปลายทางเป็น External Storage ของเรา เพื่อป้องกันไม่ให้มีการเขียนข้อมูลลงบนหลักฐานต้นทางที่เราจะเก็บ\
&#x20;      8.2 **Image Filename :** ตั้งชื่อไฟล์ Image โดยไม่ต้องใส่นามสกุลไฟล์\
&#x20;      8.3 **Image Fragment Size :**  หากต้องการทำ Fragment ให้ใส่ Size ของ Image ที่ต้องการแบ่งในแต่ละ Fragment แต่หากไม่ต้องการทำ Fragment ให้ใส่ 0\
&#x20;      8.4 **Compression :** เลือกระดับของการบีบอัดไฟล์ Image โดยหากต้องการให้บีบอัดให้ไฟล์ Image ให้มีขนาดเล็ก จะต้องใช้เวลาในการบีบอัดที่นานในการเก็บหลักฐาน (เลือกได้ตั้งแต่ 0-9)\
&#x20;      8.5 **Use AD Encryption :** หากต้องการให้ทำ Encryption ไฟล์ Image ด้วยให้ติ๊กถูก

หลังจากใส่ข้อมูลต่างๆเรียบร้อยแล้ว ให้คลิกที่ Finish&#x20;

![Select image destination](<../.gitbook/assets/image (16) (1).png>)

&#x20; 9\. จะเห็นว่ามี Image Destination ขึ้นมาแล้ว หากเราต้องการเก็บ Evidence ลงไปหลายๆที่ก็สามารถกด Add ต่อได้\
จากนั้น **ให้เลือก Verify images after they are created หากต้องการทำ Verify หลักฐาน** โดยจะมีการทำ Hashing และเทียบค่า Hash ของหลักฐาน กับไฟล์ Image ว่ามีค่าเท่ากันหรือไม่ เพ่ือเช็คความสมบูรณ์ถูกต้องของหลักฐานที่เก็บมา ( Intregity ) จากนั้นคลิกที่ Start

![Start to create image](<../.gitbook/assets/image (17) (1).png>)

&#x20; 10\. เมื่อคลิกที่ Start แล้วจะเข้าสู่การเก็บหลักฐานโดยสร้าง Image ไฟล์ลงบน Device ปลายทางให้รอจนเสร็จ

![Creating image...](<../.gitbook/assets/image (18).png>)

&#x20; 11\. หลังจากเก็บหลักฐานเป็นไฟล์ Image เสร็จก็จะเข้าสู่ขั้นตอนการ Verify หลักฐานว่าถูกต้องครบถ้วนหรือไม่ (ในกรณีที่เลือก **Verify images after they are created ในขั้นตอนที่ 11**)

![Verifying..](<../.gitbook/assets/image (19) (1).png>)

&#x20; 12\. เสร็จแล้วจะขึ้นผลของการ Verify Evidence ว่าข้อมูลหลักฐานกับ Image file ตรงกันหรือไม่ รวมถึงมีการหา Bad sector ให้คลิกที่ Close&#x20;

{% hint style="info" %}
Bad sector : คือพื้นที่บน Disk ในส่วนที่มีความเสียหาย ไม่สามารถใช้งานหรือเก็บข้อมูลได้้ ซึ่งโดยปกติทั่วไปหากพบ Bad sector บนหลักฐานที่พบ โปรแกรมเก็บหลักฐานจะทำการแทนที่ข้อมูลที่อยู่บนส่วนนั้นด้วยค่า 0 แทน
{% endhint %}

![Verify Image](<../.gitbook/assets/image (20).png>)

&#x20; 13\. จะขึ้น Status ว่าได้ทำการสร้างไฟล์ Image เป็นที่เรียบร้อยแล้ว ให้ทำการกดที่ Close ก็จะเสร็จสิ้นขั้นตอนการเก็บหลักฐานประเภท Non-volatile โดยจะได้ผลลัพธ์ออกมาเป็น 2ไฟล์ โดยเป็น Image ของหลักฐานที่เก็บมาและไฟล์ที่บอกเกี่ยวกับข้อมูลของหลักฐานที่เก็บมา

![Image created successfully](<../.gitbook/assets/image (21) (1).png>)

![Image file](<../.gitbook/assets/image (22).png>)
