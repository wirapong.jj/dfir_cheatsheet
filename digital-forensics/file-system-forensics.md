# File System Forensics

ก่อนอื่นที่เราจะทำ File System forensics ได้เราจะต้องรู้เกี่ยวกับพื้นฐานต่างๆกันก่อน

## Part 1. Overview

### Harddisk Components

![HDD Components](../.gitbook/assets/image.png)

ส่วนประกอบหลักของ Harddisk มีดังนี้

* **Actuator :** ควบคุมการหัว อ่าน/เขียน โดยใช้มอเตอร์ขนาดเล็ก
* **Read/Write head :** มีไว้สำหรับเขียน/อ่าน ข้อมูลที่อยู่บน Platter&#x20;
* **Platter :** จานแผ่นกลมๆ โดยทั่วไปจะทำจาก อลูมิเนียม/เซรามิก ซึ่งมีพื้นผิวเป็นแม่เหล็ก โดยใช้เพื่อเก็บข้อมูลซึ่งการเขียน/อ่านข้อมูล จะทำบน Platter ทั้งสองด้าน
* **Spindle :** ควบคุมการหมุนของ Platter เพื่อให้ Read/Write head สามารถเข้าถึงข้อมูลในส่วนที่ต้องการ เขียน/อ่าน ที่ต้องการได้ โดยใช้มอเตอร์ขนาดเล็ก

### Track, Cluster, Sector

![Track, Cluster, Sector](<../.gitbook/assets/image (1) (1).png>)

ในแต่ละ Platter จะมีการแบ่งเป็น Track ซึ่งทั่วไปจะมีทั้งหมด 1024 Track บน 1 Platter โดย Track ที่ 0 จะอยู่วงนอกสุดของ Platter และจะเรียงเข้าไปจนถึงส่วน Track ที่ 1023 ที่อยู่ติดกับ Spindle&#x20;

และมีหน่วยย่อยลงไปอีกในแต่ละ Track ดังนี้

* **Sector :** เป็นหน่วยที่มีเล็กที่สุดบน disk​ ซึ่งโดยปกติแล้วจะมีขนาดเท่ากับ 512 bytes&#x20;
* **Cluster (บน Linux จะเรียกว่า Block ) :** เป็นหน่วยที่ใช้เรียกกลุ่มของ Sector บน Harddisk โดยค่าที่ต่ำสุดที่สามารถแบ่ง Cluster ได้คือ 1  Sector / Cluster

### Allocated space, Unallocated space, Slack space

สถานะของ Space หลักๆ จะมีดังนี้

* **Allocated space :** เป็นพื้นที่ที่ถูกจองเพื่อใช้งานแล้ว ไม่สามารถเขียนข้อมูลอื่นทับลงพื้นที่นี้ได้ เนื่องจากมีการใช้งานอยู่
* **Unallocated space :** เป็นพื้นที่ที่ยังไม่ถูกจอง สามารถเขียนข้อมูลลงพื้นที่นี้ได้ ส่วนมาก Unallocated space จะเกิดขึ้นเมื่อมีการลบไฟล์ โดยข้อมูลที่อยู่บน Space ในส่วนนี้จะยังไม่ได้ถูกลบจริงๆเป็นเพียงแค่การเปลี่ยนสถานะว่าพื้นที่ไม่ได้ใช้งานแล้ว สามารถมาเขียนข้อมูลอื่นๆทับได้ **ซึ่งหากยังไม่มีการเขียนทับข้อมูลบนพื้นที่ส่วนนี้ก็จะสามารถทำ Recovery ไฟล์ ( กู้ไฟล์ ) ที่อยู่บนพื้นที่ส่วนนี้กลับมาได้**
* **Slack space :** เป็นพื้นที่ในส่วนที่เหลือจากการใช้งาน เช่นมี Cluster มีขนาด 1024 bytes แต่ไฟล์มีขนาดเพียง 824 bytes ก็จะเหลือพื้นที่ที่เป็น Slack space ขนาด 200 bytes **ซึ่งหากพื้นที่ในส่วน Slack space เคยถูกใช้งานในการเก็บข้อมูลก่อนหน้านี้แล้วอาจทำให้มีข้อมูลเก่าเหลืออยู่บนพื้นที่นี้ได้เช่นกัน**

เพื่อให้เข้าใจภาพรวมทั้งหมด จะยกตัวอย่างดังนี้

1. ทำการ Format Harddisk และแบ่งให้ 1 Cluster มีพื้นที่เท่ากับ 2048 bytes ดังนั้น\
   เท่ากับว่ามี 4 Sector / Cluster **โดยพื้นที่ทั้งหมดยังไม่ได้ถูกใช้งาน ก็จะเรียกว่าเป็น Unallocated space ( พื้นที่ที่ยังไม่ได้ถูกจอง )** ตามภาพด้านล่าง\
   \
   512 bytes = 1 Sector\
   2048 bytes = **4 Sector (** 2048 bytes /512 bytes )

![Format Disk](<../.gitbook/assets/image (2) (1).png>)

&#x20;  2\. จากนั้นได้ทำการสร้างไฟล์ A ขนาด 3000 bytes ดังนั้นจะต้องใช้ทั้งหมด 2 Cluster ( 4096 bytes ) ในการเขียนข้อมูลไฟล์ **ซึ่งการจองพื้นที่เพื่อเขียนข้อมูลของไฟล์ก็จะเรียกว่าเป็น Allocated space ( พื้นที่ที่ถูกจองแล้ว )**

ซึ่งหลังจากที่เราคำนวนดูจะพบว่ามีพื้นที่ส่วนที่เหลือจำนวน 1096 bytes ( 4096 bytes - 3000 bytes ) **โดยพื้นที่ในส่วนที่เหลือนี้จะไม่ถูกใช้งานในการเขียนข้อมูลใดๆ ซึ่งก็จะเรียกพื้นที่ในส่วนนี้ว่า Slack Space** ตามภาพด้านล่าง

![Write file A](<../.gitbook/assets/image (3) (2).png>)

&#x20;   3\. จากนั้นได้ทำการลบไฟล์ A ก็จะเท่ากับว่า พื้นที่ทั้งหมดจะกลับไปมีสถานะเป็น Unallowcated space **แต่ข้อมูลจริงๆที่อยู่บน Harddisk นั้นไม่ได้ถูกลบเพียงแค่บอกว่าพื้นที่ส่วนนี้ไม่ได้ถูกใช้งานแล้วสามารถเขียนทับได้ ซึ่งหากยังไม่มีข้อมูลอื่นมาเขียนทับ เราจะสามารถกู้ไฟล์ A กลับมาได้** ตามภาพด้านล่าง

![After delete file A](<../.gitbook/assets/image (4) (1).png>)

&#x20; 4\. จากนั้นได้ทำการสร้างไฟล์ B ขนาด 2200 bytes ขึ้นมาบนเครื่อง ซึ่งได้มีการเขียนข้อมูลของไฟล์ B ลงบน Cluster เดิมที่ไฟล์ A เคยอยู่พอดี **โดยจะเห็นได้ว่าข้อมูลไฟล์ B จะไม่ได้เขียนทับพื้นที่เก่าที่เคยใช้บนไฟล์ A ทั้งหมดเนื่องจากมีขนาดไฟล์เพียง 2200 bytes จะทำให้เหลือส่วนที่เป็นข้อมูลของไฟล์ A ซึ่งส่วนนี้เรียกว่า Slack space ของไฟล์ A** ซึ่งมีขนาด 800 bytes ( 3000 bytes - 2200 bytes ) ซึ่งสามารถเราดูข้อมูลของไฟล์ A ที่ยังไม่ได้ถูกเขียนทับได้ แต่ไม่สามารถกู้ไฟล์ A กลับมาแบบครบถ้วนได้ เพราะพื้นที่บางส่วนถูกไฟล์ B เขียนทับไปแล้วนั่นเอง&#x20;

![After write file B](<../.gitbook/assets/image (5).png>)

### File System Architectures&#x20;

หลายๆคนที่ใช้ Storage น่าจะเคยคุ้นกับ FAT12, FAT16, FAT32, NTFS คำถามคือแล้วมันแตกต่างกันอย่างไรล่ะ ?&#x20;

* **FAT12 ( 12-bit File Allocation Table )**&#x20;
  * Cluster Size มีขนาดได้ตั้งแต่ 512 bytes ถึง 8 KB
  * สามารถมี Address ได้ 12 bit ( ตามชื่อ )&#x20;
  * มี Cluster ได้สูงสุด 4,096 Cluster ( 2 ยกกำลังด้วย 12 bit-address ) ดังนั้นในทางทฤษฎีจะมี Volume size สูงสุดได้ 32​ MB ( 4,096 Cluster x 8 KB  )&#x20;
* **FAT16 ( 16-bit File Allocation Table )**&#x20;
  * Cluster Size มีขนาดได้ตั้งแต่ 512 bytes ถึง 64 KB
  * สามารถมี Address ได้ 16 bit ( ตามชื่อ )&#x20;
  * มี Cluster ได้สูงสุด 65,536 Cluster ( 2 ยกกำลังด้วย 16 bit-address ) ดังนั้นในทางทฤษฎีจะมี Volume size สูงสุดได้ 4​ GB ( 65,536 Cluster x 64 KB  )
* **FAT32 ( 32-bit File Allocation Table )**&#x20;
  * Cluster Size มีขนาดได้ตั้งแต่ 512 bytes ถึง 32 KB
  * สามารถมี Address ได้ 28 bit ( ตามชื่อเป็น 32 bit แต่จริงๆมีเพียง 28 bit อีก 4 ปล่อยทิ้งไว้ )
  * มี Cluster ได้สูงสุด 268,435,456 Cluster ( 2 ยกกำลังด้วย 28 bit-address ) ดังนั้นในทางทฤษฎีจะมี Volume size สูงสุดได้ 8 TB ( 268,435,456 Cluster x 32 KB )&#x20;

{% hint style="info" %}
ซึ่งเริ่มแรก FAT นั้นจะมีข้อจำกัดเรื่องการตั้งชื่อไฟล์ในรูปแบบ 8.3 filename ( การตั้งชื่อไฟล์จะต้องมีความยาวของตัวอักษรไม่เกิน 8 ตัวอักษร และมีความยาวของนามสกุลไฟล์ไม่เกิน 3 ตัวอักษร ) แต่ในภายหลัง บนระบบปฎิบัติการ Windows 95 ได้มีการพัฒนา VFAT เพื่อมาช่วยในเรื่องของ Long File Names (LFNs) ให้สามารถตั้งชื่อไฟล์ได้ยาวขึ้นถึง 255 ตัวอักษร และยังสามารถรองรับ 8.3 filename ( backward compatible ) ด้วยเช่นกัน สามารถดูรายละเอียดเพิ่มเติมได้ใน [VFAT](https://en.wikipedia.org/wiki/Design\_of\_the\_FAT\_file\_system#VFAT)\
\
และนอกจากนั้นยังมีการเก็บ File Allocation Table ลงบนพื้นที่ที่ถูก Fixed ไว้บนตำแหน่งที่ใช้ในการเก็บ File Allocation Table โดยเฉพาะ&#x20;
{% endhint %}

จากนั้นได้มีการพัฒนา File System Architecture ใหม่ โดย Microsoft ​​ ซึ่งใช้ชื่อว่า NTFS และมีการใช้กันจนถึงทุกวันนี้ โดย NTFS มีคุณสมบัติดังนี้

* **NTFS ( New Technology File System )**&#x20;
  * Cluster Size มีขนาดสูงสุดได้ถึง 64 KB
  * ในทางทฤษฎีจะมี Volume size ได้สูงสุดถึง 256 TB

{% hint style="info" %}
นอกจากนั้น NTFS มีความสามารถในการทำ Compression, Encryption, Object Permissions ซึ่งมีการใช้ MFT ( Master File Table ) ในการจัดเก็บข้อมูลเกี่ยวกับไฟล์ต่างๆ แทนที่การเก็บ File Allocation Table แบบ FAT โดยจะมีการจัดเก็บ MFT ในรูปแบบของไฟล์ โดยสามารถอ่านข้อมูลเพิ่มเติมได้ที่ [MFT wiki](https://en.wikipedia.org/wiki/NTFS#Master\_File\_Table)
{% endhint %}

{% hint style="warning" %}
สำหรับ Volume size ที่กล่าวไปข้างต้นนั้นเป็นเพียงการคำนวนทางทฤษฎี ซึ่งในทางปฎิบัติจริงๆจะมีข้อจำกัดในแต่ละระบบปฎิบัติการเวอร์ชั่นต่างๆอยู่ เช่น FAT32 นั้นหากคิดตามทฤษฎีจะสามารถสร้าง Volume ที่มีขนาดสูงสุดได้ 8TB แต่ในทางปฎิบัติหากใช้ Windows 7 จะสร้าง Volume ได้สูงสุดเพียง 32 GB เท่านั้น สามารถอ่านเพิ่มเติมได้ที่ [Microsoft Default cluster size](https://support.microsoft.com/en-us/help/140365/default-cluster-size-for-ntfs-fat-and-exfat)
{% endhint %}

### Master Boot Record

ในตอนที่เปิดเครื่อง จะรู้ได้อย่างไรว่าจะต้องเริ่มทำงานที่ Partition ใด ?&#x20;

สิ่งที่จะบอกได้คือ MBR ( Master Boot Record ) ซึ่งจะเก็บอยู่บน Sector แรก ของ Harddisk เสมอ ซึ่งมีไว้สำหรับเก็บข้อมูล Partition ต่างๆที่มีอยู่บนเครื่อง และจะเก็บข้อมูลว่าให้เริ่มทำงานที่ Partition ใด ซึ่งการอ่านข้อมูลของ MBR สามารถดูได้ตามภาพด้านล่างเลยครับ

![MBR](<../.gitbook/assets/image (6) (1).png>)

### File System Layer&#x20;

สำหรับ File System เองจะมีการแบ่ง Layer ของ File System ( ให้นึกภาพคล้ายๆกับการแบ่ง Layer ของ Network ) จะมีทั้งหมด 5 Layer ดังนี้

* **Physical Layer :** ตัว Drive เอง
* **File System Layer :** ข้อมูลเกี่ยวกับ Partition
* **Data Layer :** ข้อมูลที่ถูกเก็บไว้บน Block, Cluster
* **Metadata Layer :** ข้อมูลเกี่ยวกับไฟล์
* **Filename Layer :** ชื่อไฟล์, ไดเรกทอรี่

## &#x20;Part 2. Get ready to analyze disk image - Sleuthkit command

หลังจากที่เรารู้พื้นฐานในเรื่องทฤษฎีกันแล้ว ใน Part นี้เราจะมาเรียนรู้คำสั่งเบื้องต้นสำหรับการใช้งาน Sleuthkit ที่เป็น Open-source tool เพื่อ Analyze disk images กัน

### Download and installation&#x20;

Mac OS สามารถใช้ brew ในการติดตั้งได้เลย โดยใช้คำสั่งตามนี้

```
$ brew install sleuthkit
```

{% hint style="info" %}
หากเป็น Windows สามารถ Download ได้ที่ [Sleuthkit](http://www.sleuthkit.org/sleuthkit/download.php)
{% endhint %}



### Display partition table ( Volume System Tools ) - mmls

ทุกๆครั้งที่เราจะทำการ Analyze จะต้องรู้ Offset ของ Partition และ File System Architectures ก่อนเสมอ โดยใช้คำสั่ง

```
$ mmls image
```

###

### Display general details of file system - fsstat

หลังจากที่เรารู้ Offset และ File System Architectures ของ Partition ที่เราจะทำการ Analyze แล้ว เราสามารถแสดงข้อมูลของ File System บน Image ที่เราเก็บมาได้ โดยใช้คำสั่ง

```
$ fsstat -f [file_type] -o [offset] image
```

###

### List file and directory names ( File Name Layer Tools ) - fls

การ List File, Directory ให้ใช้คำสั่ง

```
$ fls -f [file_type] -o [offset] image
```

{% hint style="info" %}
Useful options:

\-r : แสดงข้อมูลแบบ Recursively\
\-d : แสดงเฉพาะไฟล์ที่ถูกลบ\
\-D : แสดงเฉพาะ Directory
{% endhint %}

การใช้คำสั่ง fls จะมีข้อมูลออกมา 4 Column ซึ่งมีรายละเอียดดังนี้

**Column 1 : File Type**&#x20;

* \-: Unknown type
* r: Regular file
* d: Directory
* c: Character device
* b: Block device
* l: Symbolic link
* p: Named FIFO
* s: Shadow
* h: Socket
* w: Whiteout
* v: TSK Virtual file / directory (not a real directory, created by TSK for convenience).

โดย File Type นั้นจะนำข้อมูลจาก 2 แหล่งมาแสดง ดังนี้\
1\. ตัวแรกคือข้อมูลที่มาจาก file name structure\
2\. ตัวที่สองคือข้อมูลที่มาจาก metadata structure&#x20;

**Column 2 : \* ( Flag )**

หากมีเครื่องหมาย \* หมายความว่า File ดังกล่าวมีข้อมูลอยู่ใน Unallocated space สามารถทำการกู้ข้อมูลของไฟล์นั้นกลับมาได้

**Column 3 : Metadata Address**

Metadata Address ของไฟล์ ​ซึ่ง  Sleuthkit จะใช้ข้อมูลในส่วนนี้อ้างอิงถึงข้อมูลไฟล์ โดยจะมีรูปแบบดังนี้

**ADDR-TYPE-ID**

* **ADDR :** Metadata Address ( Entry )
* **TYPE :** Attribute type&#x20;
* **ID :** Attribute id

{% hint style="info" %}
สามารถไปอ่านข้อมูลเพิ่มเติมเกี่ยวกับ Attribute type ใน MFT Entry ได้ใน​ [MFT](http://www.c-jump.com/bcc/t256t/Week04NtfsReview/W01\_0240\_mft\_attribute\_types.htm)
{% endhint %}

**Column 4 : File/Directory Name**

ยกตัวอย่างเพื่อให้เห็นภาพง่ายๆ เช่น&#x20;

**-/r \* 29-128-3: file.dat**

**-/r**  หมายความว่า File Type ของไฟล์นี้ไม่มีข้อมูลบน file name structure แต่มีข้อมูลบน metadata structure ( ซึ่งในกรณี Image ที่เราเอามาทำนั้นใช้ NTFS ดังนั้น ข้อมูลเกี่ยวกับไฟล์ที่หลงเหลืออยู่บน metadata structure ก็คือ MFT ) เนื่องจากมีการลบไฟล์ดังกล่าวไปเรียบร้อยแล้ว ทำให้ข้อมูล file type ทื่อยู่บน file name structure นั้นหายไป โดยข้อมูลที่มีอยู่บนบ่งบอกถึงว่าไฟล์ดังกล่าวเป็น Regular file&#x20;

**\*** ไฟล์ดังกล่าวถูกลบ แต่มีข้อมูลอยู่บน Unallocated space สามารถทำการกู้กลับมาได้

**29-128-3**

* Metadata Address = 29
* Attribute type = 128 ( type 128 จะอ้างอิงถึง $DATA ซึ่งจะเก็บข้อมูลของไฟล์หรือที่เรียกว่า File Content )&#x20;
* Attribute id = 3



#### **การ List File, Directory  ที่อยู่ใน Directory มี 2 รูปแบบคือ**

#### **\* รูปแบบย่อ :** ให้ทำการระบุ Metadata Address ของ Directory นั้น ตามคำสั่งด้านล่าง

```
$ fls -f [file_type] -o [offset] image metadata_address
```

{% hint style="info" %}
ซึ่งการทำงานของคำสั่ง fls มันจะทำการเข้าไปอ่านข้อมูลโดยใช้ **$INDEX\_ROOT เพื่อใช้ในการ List File, Directory ที่อยู่ภายใต้ Root node ดังกล่าว (** Attribute type 144 - เป็นข้อมูลของ Root node ของ index tree **)**&#x20;
{% endhint %}

&#x20;**2. รูปแบบเต็ม : ใ**ห้ทำการระบุ Metadata Address, Attribute type, Attribute id ของ Directory นั้น ตามคำสั่งด้านล่าง

```
$ fls -f [file_type] -o [offset] image metadata_address-attribute_type-attribute_id
```

{% hint style="danger" %}
หากเรามาลองใช้ fls กับ regular file ( Metadata Address = 29 ) จะไม่สามารถใช้ fls กับ regular file ได้เนื่องจากหา $INDEX\_ROOT ของไม่เจอ ตามภาพด้านล่าง
{% endhint %}

![$INDEX\_ROOT not found](<../.gitbook/assets/image (20) (1).png>)

#### File Searching

นอกจากนั้นเราสามารถหาไฟล์ ด้วยการใช้ชื่อไฟล์ในการค้นหาได้ โดยใช้ grep ตามคำสั่งนี้

```
$ fls -r -f [file_type] -o [offset] image | grep word
```

ตัวอย่างเช่น ทำการหาไฟล์ที่มีนามสกุลเป็น .php

![PHP file](<../.gitbook/assets/image (21).png>)

{% hint style="info" %}
หากต้องการข้อมูลเพิ่มเติมสามารถไปอ่านได้ใน [fls](http://wiki.sleuthkit.org/index.php?title=Fls#File\_Name)
{% endhint %}

###

### **Display / Recover file contents (** Meta Data Layer Tools ) - **icat**

#### Display file contents

เราสามารถแสดงเนื้อหาของไฟล์ได้ โดยใช้คำสั่ง icat โดยมีัสองรูปแบบคือ

1. **รูปแบบย่อ :** ให้ทำการระบุ Metadata Address ของไฟล์

```
$ icat -f [file_type] -o [offset] image metadata_address
```

{% hint style="info" %}
ซึ่งการทำงานของคำสั่ง icat มันจะทำการเข้าไปอ่านเนื้อหาของไฟล์ โดยใช้ **$DATA** ( Attribute type 128 - File Content )&#x20;
{% endhint %}

&#x20;**2. รูปแบบเต็ม :** ให้ทำการระบุ Metadata Address, Attribute type, Attribute id ของไฟล์ ตามคำสั่งด้านล่าง

```
$ icat -f [file_type] -o [offset] image metadata_address-attribute_type-attribute_id
```

#### File Recovery ( กู้ไฟล์ ) / Export file&#x20;

&#x20;เราสามารถใช้ icat ทำการ export file หรือกู้ไฟล์ที่ถูกลบ เพียงแค่ใช้เครื่องหมาย **>** ในการเขียนออกมาเป็นไฟล์ ตามคำสั่งด้านล่าง

```
$ icat -f [file_type] -o [offset] image metadata_address > filename
```

###

### Create file system timeline **- fls, mactime**

หากเราทำเคสที่เกี่ยวกับการโจมตีในบางครั้ง เราอาจจะต้องการหาว่า ในช่วงเวลาที่ Attacker เข้ามาวางไฟล์นั้น มีไฟล์อื่นๆเพิ่มเติม ที่ Attacker ได้มีการวางอีกหรือไม่ ซึ่งสิ่งที่จะมาตอบได้ก็คือ File system timeline โดยการทำ File system timeline มีขั้นตอนดังนี้

1. ใช้ fls ในการ List ข้อมูลทั้งหมดออกมา ( recursive ) แบบ time machine format โดยใช้คำสั่ง

```
$ fls -f [file_type] -o [offset] -r -m mnt image > image.body
```

{% hint style="info" %}
options -m สามารถใส่ได้ mount point ได้เพื่อให้ง่ายตอนดู Timeline ( เช่น C:, /usr หรืออื่นๆ )
{% endhint %}

&#x20;  2\. ใช้ mactime ในการสร้าง timeline&#x20;

```
$ mactime -b body -d > fs_timeline.csv
```



### File Carving on Unallocated space / Slack space ( Data Unit Layer Tools ) - blkls, foremost&#x20;

การทำ File carving คือการนำ Header/footer ( hex ) ของไฟล์แต่ละชนิด ไปทำการค้นหาและทำการกู้คืนไฟล์กลับมา

{% hint style="info" %}
ดูรายละเอียดเกี่ยวกับ Header/footer ของแต่ละแบบได้ที่ [File signatures](https://www.garykessler.net/library/file\_sigs.html)
{% endhint %}

1. เริ่มจากการนำข้อมูลที่อยู่บน Unallocated space ออกมาก่อน โดยใช้ blkls&#x20;

```
$ blkls image > image.unallocated
```

&#x20;  2\. นำข้อมูลบน Slack space ออกมา โดยใช้ -s&#x20;

```
$ blkls -s image > image.slack
```

&#x20;  3\. จากนั้นใช้ foremost ในการทำ File carving บน Unallocated space, Slack space

```
$ foremost -i input -o output_dir
```

{% hint style="info" %}
สามารถใช้ options -t เพื่อระบุถึงประเภทของไฟล์ที่ต้องการ carving ได้เช่น jpg,png,exe
{% endhint %}



### Strings searching on Slack space&#x20;

ในบางครั้งเราอาจต้องการค้นหาข้อมูลโดยการใช้  Keyword ( Strings ) ในการ search บน Slack space สามารถทำได้โดยมีขั้นตอนดังนี้

1. นำข้อมูลบน Slack space ออกมา โดยใช้ blkls

```
$ blkls -s image > image.slack
```

&#x20; 2\. จากนั้นจะต้องแปลงข้อมูลให้อยู่ในรูปแบบ Strings โดยใช้คำสั่งตามนี้

```
$ strings -a -t d image.slack > image.strings
```

3\. ใช้ grep ในการค้นหา Strings&#x20;

```
$ grep word image.strings
```

{% hint style="info" %}
ซึ่งเราสามารถ Search strings บน Space ในแบบอื่นๆ เพื่อหาว่า Strings นี้อยู่บนไฟล์ไหนได้ด้วย​ ซึ่งสามารถอ่านเพิ่มเติมได้ที่ [FS Analysis](https://wiki.sleuthkit.org/index.php?title=FS\_Analysis)
{% endhint %}



## Part 3. Analyze disk image - [**ashemery**](https://www.ashemery.com/dfir.html) **( Challenge #1 - Web Server Case )**

หลังจากที่รู้พื้นฐานต่างๆแล้ว เรามาลองลงมือทำ File system forensics บน Image  [**ashemery**](https://www.ashemery.com/dfir.html) **( Challenge #1 - Web Server Case )**&#x20;

โดย Image ดังกล่าวเกี่ยวกับการโจมตีทาง Web service ดังนั้นจะยกตัวอย่างด้วยการไปดู log ของ Web-service กัน

เริ่มแรกให้ใช้ mmls เพื่อแสดง Partition ทั้งหมด โดยจะเห็นได้ว่า Partition ที่เราต้องการ Analyze เป็น NTFS ซึ่งอยู่ที่ Offset 2048

![Partition](<../.gitbook/assets/image (7).png>)

จากนั้นลองใช้ fls เพื่อแสดงไฟล์ใน Partition ดังกล่าวดู โดยการระบุประเภทของ Partition และ Offset จะเห็นว่า xampp นั้นมีค่า Metadata address เป็น 42729 ตามภาพด้านล่าง

![List of file/directory ](<../.gitbook/assets/image (8).png>)

จากนั้นให้ List file/Directory ใน xampp โดยการใช้ Metadata address เป็น 42729 บนคำสั่ง fls ก็จะเห็นว่า apache นั้นมีค่า Metadata address เป็น 42739 ตามภาพด้านล่าง จากนั้นให้ทำไปเรื่อยๆจนเข้าถึง logs

![](<../.gitbook/assets/image (9).png>)

![](<../.gitbook/assets/image (10) (1).png>)

จะเห็นได้ว่ามีไฟล์ที่อยู่บน logs ทั้งหมด 5 ไฟล์ ตามภาพด้านล่าง

![](<../.gitbook/assets/image (11) (1).png>)

ซึ่งไฟล์ที่เราต้องการก็คือ access.log, error.log ซึ่งเราสามารถอ่านเนื้อหาของไฟล์ได้โดยการใช้ icat ตามภาพด้านล่าง

![](<../.gitbook/assets/image (12) (1).png>)

![](<../.gitbook/assets/image (13).png>)

หรือหากต้องการเขียนออกมาเป็นไฟล์ ก็สามารถทำได้ โดยการใช้เครื่องหมาย >

![](<../.gitbook/assets/image (14).png>)

ลองค้นหาไฟล์ php แปลกๆที่ถูกเรียกดู โดยหาจาก access.log

![](<../.gitbook/assets/image (15) (1).png>)

ก็จะพบว่ามีไฟล์ชื่อแปลกๆ เช่น  phpshell.php, phpshell2.php, c99.php,tmpukudk.php, tmpudvfh.php ซึ่งลองไปดูสักไฟล์ละกัน&#x20;

โดยก่อนอื่นที่เราจะไปดูเนื้อหาไฟล์ได้ เราจะต้องหา Metadata address ของไฟล์นั้นก่อน ซึ่งเราจะใช้ fls แบบ recursive ( -r ) เพื่อหาไฟล์จากชื่อกัน ตามภาพด้านล่าง

![](<../.gitbook/assets/image (16).png>)

จากนั้นลองมาดูเนื้อหาของไฟล์โดยใช้ icat พบว่าไฟล์ดังกล่าวเป็น Web shell ที่วางไว้เพื่อทำ Persistence

![](<../.gitbook/assets/image (17).png>)

เมื่อเราทราบว่าไฟล์นี้เป็นไฟล์ที่เป็นของ Attacker เป็นผู้นำมาวางไว้ ดังนั้นผู้ที่เรียกไฟล์ดังกล่าว จะถือว่ามีความผิดปกติทั้งหมด ไม่ใช่การใช้งานของ User ทั่วไป ดังนั้นเราก็กลับไปดูว่าบน Access log ใครมาเรียก Web shell ดังกล่าวบ้าง ก็จะเห็นว่ามี IP : 192.168.56.102 เข้ามาเรียกไฟล์ Web shell ตามภาพด้านล่าง

![](<../.gitbook/assets/image (18) (1).png>)

ดังนั้นลองดูต่อว่า IP ดังกล่าวได้เข้ามาทำอะไรกับระบบบ้างเพื่อทำ Timeline หา ROOT-CAUSE, Impact ของการโจมตี

![](<../.gitbook/assets/image (19).png>)



สำหรับบทความนี้ก็จะยกตัวอย่างเพียงคำสั่งเบื้องต้นในการใช้ Sleuthkit เท่านั้น หากต้องการทำเทคนิคอย่างอื่นเพิ่มเติมลองไปอ่านใน [TSK Tool](http://wiki.sleuthkit.org/index.php?title=TSK\_Tool\_Overview)&#x20;

## Reference

{% embed url="http://wiki.sleuthkit.org/index.php?title=TSK_Tool_Overview" %}

{% embed url="http://dftt.sourceforge.net/test7/index.html" %}

{% embed url="http://www.c-jump.com/bcc/t256t/Week04NtfsReview/W01_0240_mft_attribute_types.htm" %}

{% embed url="https://www.garykessler.net/library/file_sigs.html" %}
