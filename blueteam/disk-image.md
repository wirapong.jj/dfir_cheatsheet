# Disk Image Mounting

### Convert VMDK Image to Raw

```shell
#convert disk.vmdk to disk.raw with qemu-img
qemu-img convert -f vmdk -O raw disk.vmdk disk.raw
```

### Convert VMDK Disk Image to Raw

```shell
#convert disk.qcow2 to disk.raw with qemu-img
qemu-img convert -f qcow2 -O raw disk.qcow2 disk.raw
```

### Mount Raw Image on MacOS

```shell
#Mount disk.raw with hdiutil
hdiutil attach -readonly -imagekey diskimage-class=CRawDiskImage disk.raw
```

### Mount E01 Image on MacOS

<pre class="language-shell"><code class="lang-shell">#Mount disk.E01 to EWF with ewfmount
mkdir ewf
ewfmount -X volicon=/Library/Filesystems/osxfuse.fs/Contents/Resources/Volume.icns disk.E01 ewf
<strong>
</strong><strong>#Mount EWF with hdiutil
</strong><strong>hdiutil attach -readonly -imagekey diskimage-class=CRawDiskImage ewf
</strong></code></pre>

