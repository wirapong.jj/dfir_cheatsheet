# Table of contents

* [DFIR.CheatSheet](README.md)

## REDTEAM - PowerShell

* [Basic Powershell](redteam-powershell/redteam-powershell.md)
* [PowerShell for Pentest](redteam-powershell/powershell-for-pentest.md)

## RED VS BLUE

* [Lateral Movement](red-vs-blue/lateral-movement/README.md)
  * [Services](red-vs-blue/lateral-movement/services.md)
  * [PowerShell (Invoke-Command, PSSession)](red-vs-blue/lateral-movement/powershell-invoke-command-pssession.md)
  * [WinRS](red-vs-blue/lateral-movement/winrs.md)
  * [WMI/WMIC](red-vs-blue/lateral-movement/wmi-wmic.md)
  * [PsExec](red-vs-blue/lateral-movement/psexec.md)
  * [Scheduled Tasks](red-vs-blue/lateral-movement/scheduled-tasks.md)

## BLUETEAM CHEATSHEET

* [Disk Image Mounting](blueteam/disk-image.md)
* [Windows Event Logs](blueteam-cheatsheet/windows-event-logs/README.md)
  * [Security](blueteam-cheatsheet/windows-event-logs/security.md)
  * [Sysmon](blueteam-cheatsheet/windows-event-logs/sysmon.md)
  * [System](blueteam-cheatsheet/windows-event-logs/system.md)
  * [PowerShell](blueteam-cheatsheet/windows-event-logs/powershell.md)
  * [Terminal Services (RDP)](blueteam-cheatsheet/windows-event-logs/terminal-services-rdp.md)
  * [Windows Defender](blueteam-cheatsheet/windows-event-logs/windows-defender.md)
* [Windows Artifacts](blueteam-cheatsheet/windows-artifacts/README.md)
  * [Registry](blueteam-cheatsheet/windows-artifacts/registry.md)
  * [File System](blueteam-cheatsheet/windows-artifacts/file-system.md)

## Malware Analysis

* [Static Analysis](malware-analysis/static-analysis/README.md)
  * [File fingerprinting ( Hash )](malware-analysis/file-fingerprinting-hash.md)
  * [File fingerprinting ( Imphash )](malware-analysis/file-fingerprinting-imphash.md)
  * [Extracting strings](malware-analysis/extracting-strings.md)
  * [Dynamic Linked Libraries](malware-analysis/static-analysis/dynamic-linked-libraries.md)
  * [Reveals obfuscated strings ( Floss & StringSifter )](malware-analysis/static-analysis/reveals-obfuscated-strings-floss-and-stringsifter.md)
* [Dynamic Analysis](malware-analysis/dynamic-analysis/README.md)
  * [Malware Sandboxing - Cuckoo Sandbox](malware-analysis/dynamic-analysis/malware-sandboxing-cuckoo-sandbox.md)

## Digital Forensics

* [Evidence Acquisition](digital-forensics/evidence-acquisition.md)
* [File System Forensics](digital-forensics/file-system-forensics.md)
